﻿using UnityEngine;
using System.Collections;

public class ColorManager : MonoBehaviour {

    public SpriteRenderer Red;
    public SpriteRenderer Orange;
    public SpriteRenderer Yellow;
    public SpriteRenderer Green;
    public SpriteRenderer Blue;
    public SpriteRenderer Violet;
    public SpriteRenderer Black;

    public float redAmt;
    public float orangeAmt;
    public float yellowAmt;
    public float greenAmt;
    public float blueAmt;
    public float violetAmt;
    public float blackAmt;

    public float alphaCap;

    public float nextTick;
    public float tickRate;

    void Update() {
        /*if (Time.time >= nextTick)
        {
            nextTick = nextTick + tickRate;
        }*/
            Color col = Red.color;
        if (redAmt >= alphaCap) {
            float diff = redAmt - alphaCap;
            blackAmt = blackAmt + diff;
            redAmt = alphaCap;
            diff = 0;
        }
        col.a = redAmt;
        Red.color = col;
        col = Orange.color;
        if (orangeAmt >= alphaCap)
        {
            float diff = orangeAmt - alphaCap;
            blackAmt = blackAmt + diff;
            orangeAmt = alphaCap;
            diff = 0;
        }
        col.a = orangeAmt;
        Orange.color = col;
        col = Yellow.color;
        if (yellowAmt >= alphaCap)
        {
            float diff = yellowAmt - alphaCap;
            blackAmt = blackAmt + diff;
            yellowAmt = alphaCap;
            diff = 0;
        }
        col.a = yellowAmt;
        Yellow.color = col;
        col = Green.color;
        if (greenAmt >= alphaCap)
        {
            float diff = greenAmt - alphaCap;
            blackAmt = blackAmt + diff;
            greenAmt = alphaCap;
            diff = 0;
        }
        col.a = greenAmt;
        Green.color = col;
        col = Blue.color;
        if (blueAmt >= alphaCap)
        {
            float diff = blueAmt - alphaCap;
            blackAmt = blackAmt + diff;
            blueAmt = alphaCap;
            diff = 0;
        }
        col.a = blueAmt;
        Blue.color = col;
        col = Violet.color;
        if (violetAmt >= alphaCap)
        {
            float diff = violetAmt - alphaCap;
            blackAmt = blackAmt + diff;
            violetAmt = alphaCap;
            diff = 0;
        }
        col.a = violetAmt;
        Violet.color = col;
        col = Black.color;
        col.a = blackAmt;
        Black.color = col;
    }

}

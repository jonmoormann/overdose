﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public SpriteRenderer status;

    public float hourLength;
    public float attackTick;
    public int currentHour = -1;

    public int[] dailySchedule = new int[16] { 0,6,6,6,1,6,6,6,6,2,6,6,6,6,6,6};

    public float colorRateRed;
    public float colorRateOrange;
    public float colorRateYellow;
    public float colorRateGreen;
    public float colorRateBlue;
    public float colorRateViolet;

    public ColorManager cm;

    // Use this for initialization
    void Start () {
        StartCoroutine(CheckAttack());
	}

    IEnumerator CheckAttack()
    {
        while (true)
        {
            if (currentHour < 15)
            {
                currentHour++;
            }
            else
            {
                currentHour = 0;
            }
            if (dailySchedule[currentHour] != 6)
            {
                StartCoroutine(ColorAttack(dailySchedule[currentHour]));
            }

            yield return new WaitForSeconds(hourLength);
        }
        }

    IEnumerator ColorAttack(int col) {
        switch (col) {
            case 0:
                Debug.Log("Red ATTACK!");
                cm.redAmt = cm.redAmt + colorRateRed;
                while (cm.redAmt > 0) {
                    Debug.Log("Red CURED!");
                    cm.redAmt = cm.redAmt + colorRateRed;
                yield return new WaitForSeconds(attackTick);
                }
                break;
            case 1:
                Debug.Log("Orange ATTACK!");
                cm.orangeAmt = cm.orangeAmt + colorRateOrange;
                while (cm.orangeAmt > 0)
                {
                    Debug.Log("Orange CURED!");
                    cm.orangeAmt = cm.orangeAmt + colorRateOrange;
                    yield return new WaitForSeconds(attackTick);
                }
                break;
            case 2:
                Debug.Log("Yellow ATTACK!");
                cm.yellowAmt = cm.yellowAmt + colorRateYellow;
                while (cm.yellowAmt > 0)
                {
                    Debug.Log("Yellow CURED!");
                    cm.yellowAmt = cm.yellowAmt + colorRateYellow;
                    yield return new WaitForSeconds(attackTick);
                }
                break;
            case 3:
                Debug.Log("Green ATTACK!");
                cm.greenAmt = cm.greenAmt + colorRateGreen;
                while (cm.greenAmt > 0)
                {
                    Debug.Log("Green CURED!");
                    cm.greenAmt = cm.greenAmt + colorRateGreen;
                    yield return new WaitForSeconds(attackTick);
                }
                break;
            case 4:
                Debug.Log("Blue ATTACK!");
                cm.blueAmt = cm.blueAmt + colorRateBlue;
                while (cm.blueAmt > 0)
                {
                    Debug.Log("Blue CURED!");
                    cm.blueAmt = cm.blueAmt + colorRateBlue;
                    yield return new WaitForSeconds(attackTick);
                }
                break;
            case 5:
                Debug.Log("Violet ATTACK!");
                cm.violetAmt = cm.violetAmt + colorRateViolet;
                while (cm.violetAmt > 0)
                {
                    Debug.Log("Violet CURED!");
                    cm.violetAmt = cm.violetAmt + colorRateViolet;
                    yield return new WaitForSeconds(attackTick);
                }
                break;
        }
    }

    void Update () {
    }
}

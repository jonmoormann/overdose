﻿using UnityEngine;
using System.Collections;

public class PillScript : MonoBehaviour {

    public float redRate;
    public float orangeRate;
    public float yellowRate;
    public float greenRate;
    public float blueRate;
    public float violetRate;

    public ColorManager cm;

    public SideEffectManager sem;
    public int sideEffect;
    public float sePoints;
    public float seDecay;
    public float seCap;

    public void TakePill()
    { //if (Time.time >= cm.nextTick)
        {
            //cm.nextTick = cm.nextTick + cm.tickRate;
            cm.redAmt = cm.redAmt + redRate;
            if (cm.redAmt < 0) {
                float diff = -cm.redAmt;
                cm.redAmt = 0;
                cm.greenAmt = cm.greenAmt+ diff;
                diff = 0;
            }
            cm.orangeAmt = cm.orangeAmt + orangeRate;
            if (cm.orangeAmt < 0)
            {
                float diff = -cm.orangeAmt;
                cm.orangeAmt = 0;
                cm.blueAmt = cm.blueAmt + diff;
                diff = 0;
            }
            cm.yellowAmt = cm.yellowAmt + yellowRate;
            if (cm.yellowAmt < 0)
            {
                float diff = -cm.yellowAmt;
                cm.yellowAmt = 0;
                cm.violetAmt = cm.violetAmt + diff;
                diff = 0;
            }
            cm.greenAmt = cm.greenAmt + greenRate;
            if (cm.greenAmt < 0)
            {
                float diff = -cm.greenAmt;
                cm.greenAmt = 0;
                cm.redAmt = cm.redAmt + diff;
                diff = 0;
            }
            cm.blueAmt = cm.blueAmt + blueRate;
            if (cm.blueAmt < 0)
            {
                float diff = -cm.blueAmt;
                cm.blueAmt = 0;
                cm.orangeAmt = cm.orangeAmt + diff;
                diff = 0;
            }
            cm.violetAmt = cm.violetAmt + violetRate;
            if (cm.violetAmt < 0)
            {
                float diff = -cm.violetAmt;
                cm.violetAmt = 0;
                cm.yellowAmt = cm.yellowAmt + diff;
                diff = 0;
            }
            sePoints++;
        }
    }

    void Update() {
        sem.DoEffect(sideEffect, sePoints);
        sePoints = sePoints - Time.deltaTime * seDecay;
        if (sePoints < 0) {
            sePoints = 0;
        }
    }
    }
